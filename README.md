# README #

# Overview
This project was built to answer the ControlUp exercise, and it answers both parts of the exercise.
* The exercise:
  1. Metric conversions.
  2. Weather comparison.
  
Please note that the tests can be executed separately or as a suite.
The test output is shown in the console.

The project contains comments and console writes to make it easier to understand each step.

### How do I get set up? ###
* Best works on IntelliJ.
* Clone the project.
* Install Java version 11 or higher.
* Install latest maven version.
* Run the suite / single test.
* Find the TestNG Emailable report at a folder called 'test-output'.
**Please note that this project was tested over Windows 10 with successful results**

#Part 2 design explanation
* In general, the design should be:
  1. taking the temperature value out of the UI.
  2. Send an API request to a different website.
  3. Get the temperature value out of the API response.
  4. Verify the condition.
* In details:
  There is a need of 2 temperature values of 2 different websites (UI and API).
  1. First part is loading the site, insert the zipcode and extract the temperature value.
  
  2. Second part is sending an API request for a different website and saving the response.
  For that, I created a class that will handle the request -
  the class contains a method that gets several parameters to make the test more dynamic (zipcode, units).
  The method is sending the request to the endpoint which is built off of 2 parts - base URL that is a constant,
  and a dynamic query.
  after sending the request, I'm saving it as a Response type that includes the response body.
  From the response, we are initializing all the required fields (in this case, status code and Temperature).
  I'm also creating a static class that will contain all the initialized fields, so I will be able to get
  each field that I want easily.
  Then, I'm saving the API Temperature field value to the test as a variable.
  After I have both temperatures, I calculate the gap, and verifying using assertion that the gap is not 
  larger than 10%.
  