import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class BaseWebPage {
    public static void navigateToUrl(String url) {
        System.out.println("Navigate to: " + url);

        WebDriver.getInstance().get(url);
        waitForPageLoad();
    }

    protected static void waitForPageLoad() {
        System.out.println("Wait for page to load");
        JavascriptExecutor ex = WebDriver.getInstance();
        ex.executeScript("return document.readyState").equals("complete");
    }

    protected static WebElement findElement(By by, String elementName) {
        System.out.println("Find element " + elementName);
        return WebDriver.getInstance().findElement(by);
    }

    protected static void click(By selector, String elementName) {
        System.out.println("Click on " + elementName);
        findElement(selector, elementName);
        scrollToCenter(selector, elementName);
        waitECTimeoutWrapped(ExpectedConditions.visibilityOfElementLocated(selector), 60, elementName);
        waitForElementToBeClickable(selector, elementName).click();
    }

    protected static void sendKeys(By by, String keys, String elementName) {
        System.out.println("Send keys '" + keys + "' to " + elementName);
        findElement(by, elementName).sendKeys(keys);
    }

    protected static String getText(By selector, String elementName) {
        System.out.println("Get text of " + elementName);
        findElement(selector, elementName);
        scrollToCenter(selector, elementName);
        return waitECTimeoutWrapped(ExpectedConditions.visibilityOfElementLocated(selector), 60, elementName).getText();
    }

    protected static void scrollToCenter(By selector, String elementName) {
        System.out.println("Scroll to center of element " + elementName);
        String scrollElementCenter = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

        ((JavascriptExecutor) WebDriver.getInstance()).executeScript(scrollElementCenter, findElement(selector, elementName));
    }

    protected static WebElement waitForElementToBeClickable(By selector, String elementName) {
        System.out.println("Wait for element " + elementName + " to be clickable");
        return waitECTimeoutWrapped(ExpectedConditions.elementToBeClickable(selector), 30, elementName);
    }

    protected static <T> T waitECTimeoutWrapped(ExpectedCondition<T> ec, int timeout, String elementName) {
        System.out.println("Wait for expected condition for element " + elementName);
        WebDriver.getInstance().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(WebDriver.getInstance(), timeout);
        T data = wait.until(ec);
        WebDriver.getInstance().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return data;
    }

    protected static void selectByVisibleText(By selector, String visibleText, String elementName) {
        System.out.println("Select selector visible text '" + visibleText + "' of element " + elementName);
        Select format = new Select(findElement(selector, elementName));
        format.selectByVisibleText(visibleText);
    }
}