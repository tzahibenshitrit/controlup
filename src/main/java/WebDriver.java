import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriver {
    private static ChromeDriver chromeDriver = null;

    private WebDriver() {
        chromeDriver = new ChromeDriver();
    }

    public static ChromeDriver getInstance() {
        if (chromeDriver == null) {
            chromeDriver = new ChromeDriver();
        }
        return chromeDriver;
    }

    public static void resetInstance(){
        chromeDriver = null;
    }
}