public class Constants {
    public static final String OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/weather?";
    public static final String METRIC_CONVERSIONS_URL = "https://www.metric-conversions.org/";
    public static final String WEATHER_URL = "https://weather.com/";

    public static final String OPEN_WEATHER_APP_ID = "76970fc611cc9bd855ee94887d80f289";
    public static final String OPEN_WEATHER_IMPERIAL = "Imperial";
    public static final String OPEN_WEATHER_METRIC = "Metric";
}
