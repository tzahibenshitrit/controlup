import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class OpenWeatherApiRequest {

    public WeatherApi getWeather(String zipcode, String units) {
        System.out.println("Sending get weather request for zip code 20852 in USA and processing the response");

        //setting the end point
        String endpoint = Constants.OPEN_WEATHER_BASE_URL + "zip=" + zipcode + "&appid=" + Constants.OPEN_WEATHER_APP_ID + "&units=" + units;

        //sending the Get request
        RequestSpecification request = given();
        Response response = request.get(endpoint);

        System.out.println("Response: " + response.getBody().asString());

        //processing the response
        int statusCode = response.getStatusCode();

        //Verifying status code
        assertEquals(response.getStatusCode(), 200, "Wrong status code");
        System.out.println("Correct status code");

        double temperature = response.jsonPath().getDouble("main.temp");

        return new WeatherApi(temperature);
    }

    //creating a response object, for cases there are more fields to validate (wind, humidity...)
    public static class WeatherApi {
        private final double temperature;

        public WeatherApi(double temperature) {
            this.temperature = temperature;
        }

        public double getTemperature() {
            System.out.println("Temp value is " + temperature);
            return temperature;
        }
    }
}