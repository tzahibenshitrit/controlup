import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WeatherWebPage extends BaseWebPage{

    public void clickOnUnitDisplay(){
        //Verify units are Imperial
        waitECTimeoutWrapped(ExpectedConditions.elementToBeClickable(By.id("WX_WindowShade")), 60, "");
        click(By.xpath("/html/body/div[1]/div[3]/div[1]/header/div/div[2]/div[2]/button"), "Units display");
    }

    public void selectFahrenheit(){
        //Verify units are Imperial
        click(By.className("UnitSelector--UnitSelectorButtonTextF--2KVHq"), "Fahrenheit");
    }

    public void insertZipCode(String zipcode){
        System.out.println("Inserting zip code");
        String selector = "LocationSearch_input";
        waitECTimeoutWrapped(ExpectedConditions.elementToBeClickable(By.id(selector)), 30, "sad");
        sendKeys(By.id(selector), zipcode, "Search field");
    }

    public void clickOnTheFirstResult(){
        System.out.println("Clicking on the first result");
        click(By.id("LocationSearch_listbox-0"), "First result");
    }

    public double getTemp(){
        System.out.println("Getting the temperature value");
        String temperature = getText(By.className("CurrentConditions--tempValue--3a50n"), "Temperature value");
        StringBuilder builder = new StringBuilder(temperature);
        builder.delete(temperature.indexOf("°"), temperature.length());
        temperature = builder.toString();
        return Double.parseDouble(temperature);
    }
}