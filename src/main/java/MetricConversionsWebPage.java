import org.openqa.selenium.By;


public class MetricConversionsWebPage extends BaseWebPage {

    public void clickOnTemperature() {
        click(By.className("temperature"), "Temperature");
    }

    public void clickOnCelsiusToFahrenheit() {
        click(By.xpath("//a[@href='/temperature/celsius-to-fahrenheit.htm']"), "Celsius to Fahrenheit");
    }

    public void insertQueryFromValue(int queryFrom) {
        sendKeys(By.id("argumentConv"), String.valueOf(queryFrom), "query field");
    }

    public double getTemperatureConversionAnswer() {
        //Clearing the answer to get only the number
        String answer = getText(By.id("answer"), "Answer");
        StringBuilder answerBuilder = new StringBuilder(answer);
        answerBuilder.delete(0, answer.indexOf(" ") + 1);
        answer = answerBuilder.toString();
        answer = answerBuilder.substring(0, answer.indexOf("°"));
        return Double.parseDouble(answer);
    }

    public void clickOnLength() {
        click(By.xpath("//a[@href='/length-conversion.htm']"), "Length");
    }

    public void clickOnOuncesToGrams() {
        click(By.xpath("//a[@href='/weight/ounces-to-grams.htm']"), "Ounces to Grams");
    }

    public void clickOnWeight() {
        click(By.xpath("//a[@href='/weight-conversion.htm']"), "Weight");
    }

    public void clickOnMetersToFeet() {
        click(By.xpath("//a[@href='/length/meters-to-feet.htm']"), "Meters to feet");
    }

    public void selectDecimalAnswerDisplay() {
        selectByVisibleText(By.id("format"), "Decimal", "Decimal units");
    }

    public double getFeetConversionAnswer() {
        //Clearing the answer to get only the number
        String answer = getText(By.id("answer"), "Answer");
        StringBuilder answerBuilder = new StringBuilder(answer);
        answerBuilder.delete(0, answer.indexOf(" ") + 1);
        answer = answerBuilder.toString();
        answerBuilder.delete(answer.indexOf("f"), answer.length());
        answer = answerBuilder.toString();
        return Double.parseDouble(answer);
    }

    public double getOuncesConversionAnswer() {
        //Clearing the answer to get only the number
        String answer = getText(By.id("answer"), "Answer");
        StringBuilder answerBuilder = new StringBuilder(answer);
        answerBuilder.delete(0, answer.indexOf(" ") + 1);
        answer = answerBuilder.toString();
        answerBuilder.delete(answer.indexOf(".") + 3, answer.length());
        answer = answerBuilder.toString();
        return Double.parseDouble(answer);
    }
}