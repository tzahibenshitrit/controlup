public class Utils {
    public static double getCelsiusToFahrenheitCalculatedValue(double celsius) {
        double result = (celsius * 1.8) + 32;
        System.out.println("C to F conversion result " + result);
        return result;
    }

    public static double getMetersToFeetCalculatedValue(double meters) {
        double result = meters * 3.28084;
        System.out.println("Meters to Feet conversion result " + result);
        return result;
    }

    public static double getOuncesToGramsCalculatedValue(double ounces) {
        double result = ounces * 28.3495;
        System.out.println("Ounces to Grams conversion result " + result);
        return result;
    }
}
