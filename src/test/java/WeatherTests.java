import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WeatherTests extends BaseTest {
    @BeforeMethod
    @Override
    public void beforeMethod() {
        super.beforeMethod();
        BaseWebPage.navigateToUrl(Constants.WEATHER_URL);
    }

    @Test
    public void weatherTest() {
        System.out.println("Test scenario - get the temperature of a location in the USA using 2 different sites (UI and API " +
                "and check if the gap between the two temperature values is greater than 10%");

        //Getting the temperature out of the HTML
        WeatherWebPage weather = new WeatherWebPage();
        weather.clickOnUnitDisplay();
        weather.selectFahrenheit();
        weather.insertZipCode("20852");
        weather.clickOnTheFirstResult();

        double weatherTemperature = weather.getTemp();
        System.out.println("Weather.com temp value " + weatherTemperature);

        OpenWeatherApiRequest openWeatherApiRequest = new OpenWeatherApiRequest();

        //creating an API data object
        OpenWeatherApiRequest.WeatherApi weatherApiData = openWeatherApiRequest.getWeather("20852,us", Constants.OPEN_WEATHER_IMPERIAL);

        //Getting the temperature out of the API data object
        double apiTemperature = weatherApiData.getTemperature();

        //Getting the gap between the 2 sites
        double gapPercentageRate = 100 * ((apiTemperature - weatherTemperature) / weatherTemperature);
        gapPercentageRate = Math.abs(gapPercentageRate);

        assertTrue(gapPercentageRate < 10, "The gap in percentage between both sites is greater than 10%");
        System.out.println("The temperatures are: API -> " + apiTemperature + " UI-> " + weatherTemperature);
        System.out.println("The gap between both sites is less than 10% -> " + gapPercentageRate);
    }
}