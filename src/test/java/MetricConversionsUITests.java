import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MetricConversionsUITests extends BaseTest {

    @BeforeMethod
    @Override
    public void beforeMethod() {
        super.beforeMethod();
        BaseWebPage.navigateToUrl(Constants.METRIC_CONVERSIONS_URL);
    }

    @Test
    public void celsiusToFahrenheitTest() {
        System.out.println("Scenario - Convert Celsius to Fahrenheit and verify the result");

        //setting measured value
        int celsiusValue = 20;

        //Creating a web page class instance for the inner methods use
        MetricConversionsWebPage metricConversionsWebPage = new MetricConversionsWebPage();

        //getting to the desired units and inserting the measured value
        metricConversionsWebPage.clickOnTemperature();
        metricConversionsWebPage.clickOnCelsiusToFahrenheit();
        metricConversionsWebPage.insertQueryFromValue(celsiusValue);

        //getting the answer out of the web page
        double answer = metricConversionsWebPage.getTemperatureConversionAnswer();

        //converting test value to fahrenheit
        double celToFahLocalCalculatedValue = Utils.getCelsiusToFahrenheitCalculatedValue(celsiusValue);

        //verifying that both results are equal
        assertEquals(celToFahLocalCalculatedValue, answer, "Wrong Fahrenheit Value");
        System.out.println("The Fahrenheit result is correct");
    }

    @Test
    public void metersToFeetTest() {
        System.out.println("Scenario - Convert meters to feet");

        int metersValue = 20;

        MetricConversionsWebPage metricConversionsWebPage = new MetricConversionsWebPage();
        metricConversionsWebPage.clickOnLength();
        metricConversionsWebPage.clickOnMetersToFeet();
        metricConversionsWebPage.insertQueryFromValue(metersValue);
        metricConversionsWebPage.selectDecimalAnswerDisplay();

        double feetAnswer = metricConversionsWebPage.getFeetConversionAnswer();
        double metersToFeetCalculatedValue = Utils.getMetersToFeetCalculatedValue(metersValue);

        assertEquals(feetAnswer, metersToFeetCalculatedValue, "Wrong feet value");
        System.out.println("The feet result is correct");
    }

    @Test
    public void ouncesToGramsTest() {
        System.out.println("Scenario - Convert ounces to grams");

        int ouncesValue = 20;

        MetricConversionsWebPage metricConversionsWebPage = new MetricConversionsWebPage();

        metricConversionsWebPage.clickOnWeight();
        metricConversionsWebPage.clickOnOuncesToGrams();
        metricConversionsWebPage.insertQueryFromValue(ouncesValue);

        double gramsAnswer = metricConversionsWebPage.getOuncesConversionAnswer();
        double ouncesConversionAnswer = Utils.getOuncesToGramsCalculatedValue(ouncesValue);

        assertEquals(gramsAnswer, ouncesConversionAnswer, "Wrong grams answer");
        System.out.println("The grams result is correct");
    }
}