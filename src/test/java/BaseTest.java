import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;


public class BaseTest {
    public static ChromeDriver driver;

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("Starting chromedriver with pre conditions");

        WebDriverManager.chromedriver().setup();

        driver = WebDriver.getInstance();

        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("Test completed, terminating chromedriver");
        driver.quit();
        WebDriver.resetInstance();
    }
}